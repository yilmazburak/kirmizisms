﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using Toolbox.ExtensionMethods;

namespace KirmiziKareSms
{
    class Dal
    {
        public static string[] GetUyelikBilgileri(string connString)
        {
            string[] uyeBilgileri = new string[2];
            using (var conn = new MySqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    string cmdText = "select ceptelefonu, sifre from smsuyebilgileri";
                    using (var cmd = new MySqlCommand(cmdText, conn))
                    {
                        var reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            uyeBilgileri[0] = reader.ReadString("ceptelefonu");
                            uyeBilgileri[1] = reader.ReadString("sifre");
                        }
                    }
                    return uyeBilgileri;
                }
                catch (Exception ex)
                {
                    throw new Exception("Hata: " + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }
        public static string GetMusteriTel(string connString, int musteriId, string telFieldName)
        {
            string musteriTel = "";
            using (var conn = new MySqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    string cmdText = "select * from kredimusterileri where id=@musteriId";
                    using (var command = new MySqlCommand(cmdText, conn))
                    {
                        command.AddParameter("musteriId", musteriId);
                        var reader = command.ExecuteReader();
                        if (reader.Read())
                        {
                            musteriTel = reader.ReadString(telFieldName);
                        }
                    }
                    return musteriTel;
                }
                catch (Exception ex)
                {
                    throw new Exception("Hata: " + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }
        public static Dictionary<int, string> GetMusteriList(string connString, string sorgu, string telFieldName)
        {
            Dictionary<int, string> musteriList = new Dictionary<int, string>();
            using (var conn = new MySqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    string cmdText = sorgu;
                    using (var command = new MySqlCommand(cmdText, conn))
                    {
                        command.AddParameter("telefonfieldname", telFieldName);
                        var reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            int musteriId = reader.ReadInt("Id");
                            string musteriTel = reader.ReadString(telFieldName);
                            musteriList.Add(musteriId, musteriTel);
                        }
                    }
                    return musteriList;
                }
                catch (Exception ex)
                {
                    throw new Exception("Hata: " + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }
        public static long SmsSiparisLogGir(string connString, DateTime siparisTarihi, string responseKod, int toplamAlici, string sablon, string baslik, DateTime? planliTarih, string mesaj)
        {
            using (var conn = new MySqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    string cmdText = "INSERT INTO smssiparislog (siparistarihi,responsekod,toplamalici,sablonadi,baslik,planlitarih,mesaj) " +
                                    "VALUES(@siparisTarihi,@responseKod,@toplamAlici,@sablonAdi,@baslik,@planliTarih,@mesaj)";
                    using (var cmd = new MySqlCommand(cmdText, conn))
                    {
                        cmd.AddParameter("siparisTarihi", siparisTarihi);
                        cmd.AddParameter("responseKod", responseKod);
                        cmd.AddParameter("toplamAlici", toplamAlici);
                        cmd.AddParameter("sablonAdi", sablon);
                        cmd.AddParameter("baslik", baslik);
                        cmd.AddParameter("planliTarih", planliTarih);
                        cmd.AddParameter("mesaj", mesaj);
                        cmd.ExecuteNonQuery();

                        return cmd.LastInsertedId;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Hata: " + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }
        public static void SmsMusteriLogGir(string connString, int siparisId, string smsDurumKodu, int hesapId, string hesapTipi, string telefon, int sayfaNo)
        {
            using (var conn = new MySqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    string cmdText = "INSERT INTO smsmusterilog (siparisid,smsdurumkodu,hesapid,hesaptipi,telefon,sayfano) VALUES(@siparisId,@smsDurumKodu,@hesapId,@hesapTipi,@telefon,@sayfaNo)";
                    using (var cmd = new MySqlCommand(cmdText, conn))
                    {
                        cmd.AddParameter("siparisId", siparisId);
                        cmd.AddParameter("smsDurumKodu", smsDurumKodu);
                        cmd.AddParameter("hesapId", hesapId);
                        cmd.AddParameter("hesapTipi", hesapTipi);
                        cmd.AddParameter("telefon", telefon);
                        cmd.AddParameter("sayfaNo", sayfaNo);
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Hata: " + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }
    }
}
