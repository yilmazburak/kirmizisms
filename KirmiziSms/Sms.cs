﻿using RGiesecke.DllExport;
using SmsApi;
using SmsApi.Types;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace KirmiziKareSms
{
    public class Sms
    {
        [DllExport]
        public static int Kalan(
            [MarshalAs(UnmanagedType.LPWStr)] string connString)
        {
            try
            {
                string[] uyeBilgileri = Dal.GetUyelikBilgileri(connString);

                // messenger objesi
                var messenger = new Messenger(uyeBilgileri[0], uyeBilgileri[1]);

                // Rapor alıp değişkene atayalım.
                var msgObj = messenger.GetBalance();

                /////////////////////////////////////////
                // gelen cevaplar ve anlamları.
                /////////////////////////////////////////

                // Status.
                // Code ve Desc alanları yer alır. Mesajın sisteme başarılı şekilde gönderilip gönderilmediği bilgisini verir.
                // Code = 200 ve Description = OK ise mesaj sisteme başarılı şekilde ulaşmıştır. Bu mesajın iletim durumu değildir.
                // Alabileceği değerler için dokümana bakınız.
                var statusCode = msgObj.Response.Status.Code;
                var statusDesc = msgObj.Response.Status.Description;

                // Main
                // Ana Kredi. Sahip olduğunuz kredi adetidir.
                var main = msgObj.Response.Balance.Main;

                // Limit
                // Eksiye düşebileceğiniz kredi adetidir.
                var limit = msgObj.Response.Balance.Limit;

                // Kullanılabilir Kredi
                var total = main + limit;

                int kalan = Convert.ToInt32(total);

                return kalan;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        [DllExport]
        public static int Gonder(
            [MarshalAs(UnmanagedType.LPWStr)] string connString,
            int musteriId,
            [MarshalAs(UnmanagedType.LPWStr)] string telFieldName,
            [MarshalAs(UnmanagedType.LPWStr)] string baslik,
            [MarshalAs(UnmanagedType.LPWStr)] string mesaj,
            [MarshalAs(UnmanagedType.LPWStr)] string sablon)
        {
            try
            {
                string[] uyeBilgileri = Dal.GetUyelikBilgileri(connString);
                // SUBMIT
                // Birden çok numraya aynı mesaj içeriği gönderilmesini sağlar.
                // Birden çok numaraya farklı içerik göndermek için SUBMITMULTI kullanmanız gerekir. 

                // messenger objesi
                var messenger = new Messenger(uyeBilgileri[0], uyeBilgileri[1]);
                var kalan = messenger.GetBalance().Response.Balance.Main + messenger.GetBalance().Response.Balance.Limit;
                if (kalan <= 0)
                {
                    MessageBox.Show("Hata: Bakiye yetersiz, " + kalan, "Hata!");
                    return 0;
                }

                /////////////////////////////////////////
                // değişkenleri tanımlayalım.
                /////////////////////////////////////////

                // gönderilecek mesaj içeriği
                // [ZORUNLU]
                var msg = mesaj;

                // telefon numaraları
                // [ZORUNLU]
                var recipientList = new List<string>();
                string musteriTel = Dal.GetMusteriTel(connString, musteriId, telFieldName);
                recipientList.Add(String.Concat("90" + musteriTel));

                // başlık, ileri tarihli gönderim, geçerlilik süresi
                var header = new Header
                {
                    //Gönderen Adı / Başlık 
                    //[ZORUNLU]
                    From = baslik

                    //İleri tarihli gönderim yapılmak isteniyorsa girilmelidir. 
                    //[OPSİYONEL]
                    //ScheduledDeliveryTime = new DateTime(2014, 02, 28, 15, 00, 00),

                    //mesaj gönderimi başarısız ise 1440 dk boyunca tekrar denenecek ve bu süre sonunda artık gönderim denenmeyecektir. 
                    //Bu alan gönderilmez veya 0 gönderilirse default değeri 1440 olarak alır.
                    //[OPSİYONEL]
                    //ValidityPeriod = 1440
                };

                // Mesajı gönderip değişkene atayalım.
                var msgObj = messenger.Submit(msg, recipientList, header, DataCoding.Default);

                /////////////////////////////////////////
                // gelen cevaplar ve anlamları.
                /////////////////////////////////////////

                // Status.
                // Code ve Desc alanları yer alır. Mesajın sisteme başarılı şekilde gönderilip gönderilmediği bilgisini verir.
                // Code = 200 ve Description = OK ise mesaj sisteme başarılı şekilde ulaşmıştır. Bu mesajın iletim durumu değildir.
                // Alabileceği değerler için dokümana bakınız.
                var statusCode = msgObj.Response.Status.Code;
                var statusDesc = msgObj.Response.Status.Description;

                // MessageId
                // Sistemden rapor alınması için geri dönen mesaj numarasıdır.
                var msgId = msgObj.Response.MessageId;

                if (statusCode == 200)
                {
                    //Jbi sms logları
                    int lastSmsSiparisLogId = Convert.ToInt32(Dal.SmsSiparisLogGir(connString, DateTime.Now, statusCode.ToString(), 1, sablon, baslik, null, msg));
                    Dal.SmsMusteriLogGir(connString, lastSmsSiparisLogId, statusCode.ToString(), musteriId, "K", musteriTel, (mesaj.Length > 160) ? (mesaj.Length / 160) + 1 : 1);
                    return 1;
                }
                else
                {
                    MessageBox.Show("Hata kodu: " + statusCode + ", Hata mesajı: " + statusDesc, "Hata!");
                    return 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        [DllExport]
        public static int GonderToplu(
            [MarshalAs(UnmanagedType.LPWStr)] string connString,
            [MarshalAs(UnmanagedType.LPWStr)] string sorgu,
            [MarshalAs(UnmanagedType.LPWStr)] string telFieldName,
            [MarshalAs(UnmanagedType.LPWStr)] string baslik,
            [MarshalAs(UnmanagedType.LPWStr)] string mesaj,
            [MarshalAs(UnmanagedType.LPWStr)] string sablon)
        {
            try
            {
                string[] uyeBilgileri = Dal.GetUyelikBilgileri(connString);

                //jbi müşteri id ve telefonları
                //müşteri id ve telefon numarası döner
                Dictionary<int, string> musteriList = Dal.GetMusteriList(connString, sorgu, telFieldName);
                var telList = new List<string>();
                foreach (KeyValuePair<int, string> item in musteriList)
                {
                    telList.Add(item.Value);
                }

                // SUBMIT
                // Birden çok numraya aynı mesaj içeriği gönderilmesini sağlar.
                // Birden çok numaraya farklı içerik göndermek için SUBMITMULTI kullanmanız gerekir. 

                // messenger objesi
                var messenger = new Messenger(uyeBilgileri[0], uyeBilgileri[1]);
                var kalan = messenger.GetBalance().Response.Balance.Main + messenger.GetBalance().Response.Balance.Limit;
                if (kalan <= 0)
                {
                    MessageBox.Show("Hata: Bakiye yetersiz, " + kalan, "Hata!");
                    return 0;
                }

                /////////////////////////////////////////
                // değişkenleri tanımlayalım.
                /////////////////////////////////////////

                // gönderilecek mesaj içeriği
                // [ZORUNLU]
                var msg = mesaj;

                // telefon numaraları
                // [ZORUNLU]
                var recipientList = new List<string>();
                recipientList = telList;

                // başlık, ileri tarihli gönderim, geçerlilik süresi
                var header = new Header
                {
                    //Gönderen Adı / Başlık 
                    //[ZORUNLU]
                    From = baslik

                    //İleri tarihli gönderim yapılmak isteniyorsa girilmelidir. 
                    //[OPSİYONEL]
                    //ScheduledDeliveryTime = new DateTime(2014, 02, 28, 15, 00, 00),

                    //mesaj gönderimi başarısız ise 1440 dk boyunca tekrar denenecek ve bu süre sonunda artık gönderim denenmeyecektir. 
                    //Bu alan gönderilmez veya 0 gönderilirse default değeri 1440 olarak alır.
                    //[OPSİYONEL]
                    //ValidityPeriod = 1440
                };

                // Mesajı gönderip değişkene atayalım.
                var msgObj = messenger.Submit(msg, recipientList, header, DataCoding.Default);

                /////////////////////////////////////////
                // gelen cevaplar ve anlamları.
                /////////////////////////////////////////

                // Status.
                // Code ve Desc alanları yer alır. Mesajın sisteme başarılı şekilde gönderilip gönderilmediği bilgisini verir.
                // Code = 200 ve Description = OK ise mesaj sisteme başarılı şekilde ulaşmıştır. Bu mesajın iletim durumu değildir.
                // Alabileceği değerler için dokümana bakınız.
                var statusCode = msgObj.Response.Status.Code;
                var statusDesc = msgObj.Response.Status.Description;

                // MessageId
                // Sistemden rapor alınması için geri dönen mesaj numarasıdır.
                var msgId = msgObj.Response.MessageId;

                //jbi sms loglarını kaydet
                int lastSmsSiparisLogId = Convert.ToInt32(Dal.SmsSiparisLogGir(connString, DateTime.Now, statusCode.ToString(), recipientList.Count, sablon, baslik, null, msg));
                var reportList = messenger.Query(msgId).Response.ReportDetail.List; // QUERY messageid ile raporlama sağlar.
                if (statusCode == 200) //başarılı
                {
                    foreach (var musteri in musteriList)
                    {
                        Dal.SmsMusteriLogGir(connString, lastSmsSiparisLogId, statusCode.ToString(), musteri.Key, "K", musteri.Value, (mesaj.Length > 160) ? (mesaj.Length / 160) + 1 : 1);
                    }
                    return 1;
                }
                else
                {
                    MessageBox.Show("Hata kodu: " + statusCode + ", Hata mesajı: " + statusDesc, "Hata!");
                    return 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        [DllExport]
        public static int PlanliGonder(
            [MarshalAs(UnmanagedType.LPWStr)] string connString,
            int musteriId,
            [MarshalAs(UnmanagedType.LPWStr)] string telFieldName,
            [MarshalAs(UnmanagedType.LPWStr)] string baslik,
            [MarshalAs(UnmanagedType.LPWStr)] string mesaj,
            [MarshalAs(UnmanagedType.LPWStr)] string sablon,
            [MarshalAs(UnmanagedType.LPWStr)] string tarih)
        {
            try
            {
                string[] uyeBilgileri = Dal.GetUyelikBilgileri(connString);
                // SUBMIT
                // Birden çok numraya aynı mesaj içeriği gönderilmesini sağlar.
                // Birden çok numaraya farklı içerik göndermek için SUBMITMULTI kullanmanız gerekir. 

                // messenger objesi
                var messenger = new Messenger(uyeBilgileri[0], uyeBilgileri[1]);
                var kalan = messenger.GetBalance().Response.Balance.Main + messenger.GetBalance().Response.Balance.Limit;
                if (kalan <= 0)
                {
                    MessageBox.Show("Hata: Bakiye yetersiz, " + kalan, "Hata!");
                    return 0;
                }

                /////////////////////////////////////////
                // değişkenleri tanımlayalım.
                /////////////////////////////////////////

                // gönderilecek mesaj içeriği
                // [ZORUNLU]
                var msg = mesaj;

                // telefon numaraları
                // [ZORUNLU]
                var recipientList = new List<string>();
                string musteriTel = Dal.GetMusteriTel(connString, musteriId, telFieldName);
                recipientList.Add(String.Concat("90" + musteriTel));

                // başlık, ileri tarihli gönderim, geçerlilik süresi
                var header = new Header
                {
                    //Gönderen Adı / Başlık 
                    //[ZORUNLU]
                    From = baslik,

                    //İleri tarihli gönderim yapılmak isteniyorsa girilmelidir. 
                    //[OPSİYONEL]
                    ScheduledDeliveryTime = DateTimeOffset.Parse(tarih).UtcDateTime

                    //mesaj gönderimi başarısız ise 1440 dk boyunca tekrar denenecek ve bu süre sonunda artık gönderim denenmeyecektir. 
                    //Bu alan gönderilmez veya 0 gönderilirse default değeri 1440 olarak alır.
                    //[OPSİYONEL]
                    //ValidityPeriod = 1440
                };

                // Mesajı gönderip değişkene atayalım.
                var msgObj = messenger.Submit(msg, recipientList, header, DataCoding.Default);

                /////////////////////////////////////////
                // gelen cevaplar ve anlamları.
                /////////////////////////////////////////

                // Status.
                // Code ve Desc alanları yer alır. Mesajın sisteme başarılı şekilde gönderilip gönderilmediği bilgisini verir.
                // Code = 200 ve Description = OK ise mesaj sisteme başarılı şekilde ulaşmıştır. Bu mesajın iletim durumu değildir.
                // Alabileceği değerler için dokümana bakınız.
                var statusCode = msgObj.Response.Status.Code;
                var statusDesc = msgObj.Response.Status.Description;

                // MessageId
                // Sistemden rapor alınması için geri dönen mesaj numarasıdır.
                var msgId = msgObj.Response.MessageId;

                if (statusCode == 200)
                {
                    //Jbi sms logları
                    int lastSmsSiparisLogId = Convert.ToInt32(Dal.SmsSiparisLogGir(connString, DateTime.Now, statusCode.ToString(), 1, sablon, baslik, DateTime.Parse(tarih), msg));
                    Dal.SmsMusteriLogGir(connString, lastSmsSiparisLogId, statusCode.ToString(), musteriId, "K", musteriTel, (mesaj.Length > 160) ? (mesaj.Length / 160) + 1 : 1);
                    return 1;
                }
                else
                {
                    MessageBox.Show("Hata kodu: " + statusCode + ", Hata mesajı: " + statusDesc, "Hata!");
                    return 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        [DllExport]
        public static int GonderToplu(
            [MarshalAs(UnmanagedType.LPWStr)] string connString,
            [MarshalAs(UnmanagedType.LPWStr)] string sorgu,
            [MarshalAs(UnmanagedType.LPWStr)] string telFieldName,
            [MarshalAs(UnmanagedType.LPWStr)] string baslik,
            [MarshalAs(UnmanagedType.LPWStr)] string mesaj,
            [MarshalAs(UnmanagedType.LPWStr)] string sablon,
            [MarshalAs(UnmanagedType.LPWStr)] string tarih)
        {
            try
            {
                string[] uyeBilgileri = Dal.GetUyelikBilgileri(connString);

                //jbi müşteri id ve telefonları
                //müşteri id ve telefon numarası döner
                Dictionary<int, string> musteriList = Dal.GetMusteriList(connString, sorgu, telFieldName);
                var telList = new List<string>();
                foreach (KeyValuePair<int, string> item in musteriList)
                {
                    telList.Add(item.Value);
                }

                // SUBMIT
                // Birden çok numraya aynı mesaj içeriği gönderilmesini sağlar.
                // Birden çok numaraya farklı içerik göndermek için SUBMITMULTI kullanmanız gerekir. 

                // messenger objesi
                var messenger = new Messenger(uyeBilgileri[0], uyeBilgileri[1]);
                var kalan = messenger.GetBalance().Response.Balance.Main + messenger.GetBalance().Response.Balance.Limit;
                if (kalan <= 0)
                {
                    MessageBox.Show("Hata: Bakiye yetersiz, " + kalan, "Hata!");
                    return 0;
                }

                /////////////////////////////////////////
                // değişkenleri tanımlayalım.
                /////////////////////////////////////////

                // gönderilecek mesaj içeriği
                // [ZORUNLU]
                var msg = mesaj;

                // telefon numaraları
                // [ZORUNLU]
                var recipientList = new List<string>();
                recipientList = telList;

                // başlık, ileri tarihli gönderim, geçerlilik süresi
                var header = new Header
                {
                    //Gönderen Adı / Başlık 
                    //[ZORUNLU]
                    From = baslik,

                    //İleri tarihli gönderim yapılmak isteniyorsa girilmelidir. 
                    //[OPSİYONEL]
                    ScheduledDeliveryTime = DateTimeOffset.Parse(tarih).UtcDateTime

                    //mesaj gönderimi başarısız ise 1440 dk boyunca tekrar denenecek ve bu süre sonunda artık gönderim denenmeyecektir. 
                    //Bu alan gönderilmez veya 0 gönderilirse default değeri 1440 olarak alır.
                    //[OPSİYONEL]
                    //ValidityPeriod = 1440
                };

                // Mesajı gönderip değişkene atayalım.
                var msgObj = messenger.Submit(msg, recipientList, header, DataCoding.Default);

                /////////////////////////////////////////
                // gelen cevaplar ve anlamları.
                /////////////////////////////////////////

                // Status.
                // Code ve Desc alanları yer alır. Mesajın sisteme başarılı şekilde gönderilip gönderilmediği bilgisini verir.
                // Code = 200 ve Description = OK ise mesaj sisteme başarılı şekilde ulaşmıştır. Bu mesajın iletim durumu değildir.
                // Alabileceği değerler için dokümana bakınız.
                var statusCode = msgObj.Response.Status.Code;
                var statusDesc = msgObj.Response.Status.Description;

                // MessageId
                // Sistemden rapor alınması için geri dönen mesaj numarasıdır.
                var msgId = msgObj.Response.MessageId;

                //jbi sms loglarını kaydet
                int lastSmsSiparisLogId = Convert.ToInt32(Dal.SmsSiparisLogGir(connString, DateTime.Now, statusCode.ToString(), recipientList.Count, sablon, baslik, DateTime.Parse(tarih), msg));
                var reportList = messenger.Query(msgId).Response.ReportDetail.List; // QUERY messageid ile raporlama sağlar.
                if (statusCode == 200) //başarılı
                {
                    foreach (var musteri in musteriList)
                    {
                        Dal.SmsMusteriLogGir(connString, lastSmsSiparisLogId, statusCode.ToString(), musteri.Key, "K", musteri.Value, (mesaj.Length > 160) ? (mesaj.Length / 160) + 1 : 1);
                    }
                    return 1;
                }
                else
                {
                    MessageBox.Show("Hata kodu: " + statusCode + ", Hata mesajı: " + statusDesc, "Hata!");
                    return 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
    }
}
